package com.itheima.service;

import entity.Result;

import java.util.Map;
/**
 * @Description:
 * @Author: yp
 */
public interface OrderService {

    /**
     * 预约
     * @param map
     * @return
     */
    Result submit(Map map) throws Exception;

    /**
     * 根据id查询预约信息
     * @param id
     * @return
     */
    Map findById(Integer id);
}
