package com.itheima.service;

import java.util.Map;

/**
 * @Description:
 * @Author: yp
 */
public interface ReportService {

    /**
     * 获得运营数据
     * @return
     */
    Map<String,Object> getBusinessReportData() throws Exception;
}
