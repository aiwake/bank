package com.itheima.service;

import com.itheima.pojo.Permission;
import entity.PageResult;
import entity.QueryPageBean;

import java.util.List;
import java.util.Set;

/**
 * @Description:
 * @Author: yp
 */
public interface PermissionService {

    /**
     * 根据角色id查询权限
     * @param roleId
     * @return
     */
    Set<Permission> findByRoleId(Integer roleId);

    /**
     * 权限管理分页查询
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    /**
     * 新增权限
     * @param permission
     * @param checkitemIds
     */
    void add(Permission permission, Integer[] checkitemIds);

    /**
     * 根据id查询权限
     * @param id
     * @return
     */
    Permission findById(Integer id);

    /**
     * 根据权限的id查询出当前权限关联的角色的Id(查询中间表t_role_permission)
     * @param permissionId
     * @return
     */
    List<Integer> findRoleIdsByPerssionId(Integer permissionId);

    /**
     * 更新权限
     * @param permission
     * @param roleIds
     */
    void edit(Permission permission, Integer[] roleIds);

    /**
     * 删除权限
     * @param id
     */
    void deleteByIdPermission(Integer id);
}
