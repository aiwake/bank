package com.itheima.service;

import com.itheima.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: yp
 */
public interface OrderSettingService {

    /**
     * 添加预约设置
     * @param orderSettingList
     */
    void add(List<OrderSetting> orderSettingList);

    /**
     * 根据月份获得当前月的预约设置
     * @param date
     * @return
     */
    List<Map> getOrderSettingByMonth(String date);

    /**
     * 根据月份获得当前月的预约设置
     * @param date
     * @return
     */
    List<OrderSetting> getOrderSettingByMonth02(String date);

    /**
     * 更新预约设置的人数
     * @param orderSetting
     */
    void editNumberByDate(OrderSetting orderSetting);
    /**
     * 定时清理数据
     * @param lastDay
     */
    void deleteCache(String lastDay);
}
