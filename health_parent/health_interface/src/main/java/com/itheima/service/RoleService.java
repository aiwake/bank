package com.itheima.service;

import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import entity.PageResult;
import entity.QueryPageBean;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.List;
import java.util.Set;

/**
 * @Description:
 * @Author: yp
 */
public interface RoleService {

    /**
     * 根据用户id查询角色
     *
     * @param userId
     * @return
     */
    Set<Role> findByUid(Integer userId);
    /**
     * 角色数据动态维护块---huang
     */
    /**
     * 分页查询角色
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    /**
     * 根据ID删除角色
     * @param id
     */
    void del(Integer id);

    /**
     * 新增角色
     * @param role
     */
    void add(Role role,Integer[] rolemenuIds,Integer[] rolepermissionIds);

    /**
     * 编辑角色
     * @param role
     */
    void edit(Role role, Integer[] rolemenuIds,Integer[] rolepermissionIds);

    /**
     * 根据ID查询权限
     * @param id
     * @return
     */
    List<Integer> findPermissionById(Integer id);


    /**
     * 根据ID查询菜单
     * @param id
     * @return
     */
    List<Integer> findMenuById(Integer id);

    /**
     * 查询所有权限
     * @param
     * @return
     */
    Set<Permission> findPermissions();

    /**
     * 查询所有菜单
     * @param
     * @return
     */
    LinkedHashSet<Menu> findMenus();

    /**
     * 根据ID查询角色信息
     * @param id
     * @return
     */
    Role findById(Integer id);

    /**
     * 查询所有角色
     *
     * @return
     */
    List<Role> findAll();

    /**
     * 根据userId查询角色的roleIds
     *
     * @param userId
     * @return
     */
    List<Integer> findRoleIdsByUserId(int userId);
}
