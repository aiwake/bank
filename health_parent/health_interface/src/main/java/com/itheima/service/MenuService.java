package com.itheima.service;

import com.itheima.pojo.Menu;
import com.itheima.pojo.Role;
import entity.PageResult;
import entity.QueryPageBean;

import java.util.List;


public interface MenuService {
  PageResult findPage(QueryPageBean queryPageBean);

  List<Role> findAll();


    List<Menu> findParentMenuList();

  Menu findPathByParentMenuId(Integer id);

  void add(Menu menu, Integer[] checkRoleIds, Integer level);

  List<Integer> findCheckRoleIds(Integer id);

  void edit(Menu menu, Integer[] checkRoleIds, Integer level);

  void del(Integer id);
}
