package com.itheima.service;

import com.itheima.pojo.Setmeal;
import entity.PageResult;
import entity.QueryPageBean;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: yp
 */
public interface SetMealService {

    /**
     * 新增套餐
     * @param setmeal
     * @param checkgroupIds
     */
    void add(Setmeal setmeal, Integer[] checkgroupIds);

    /**
     * 查询所有的套餐
     * @return
     */
    List<Setmeal> getSetmeal();

    /**
     * 套餐详情
     * @param id
     * @param type
     * @return
     */
    Setmeal findById(Integer id, Integer type);

    List<Map<String,Object>> getSetmealReport();

    PageResult findPage(QueryPageBean queryPageBean);
}
