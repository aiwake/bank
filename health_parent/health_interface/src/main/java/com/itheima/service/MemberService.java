package com.itheima.service;

import com.itheima.pojo.Member;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: yp
 */
public interface MemberService {

    /**
     * 根据手机号码查询会员
     * @param telephone
     * @return
     */
    Member findByTelephone(String telephone);

    /**
     * 注册
     * @param member
     */
    void register(Member member);

    /**
     * 统计每个月分的会员的总数(累加)
     * @param monthList
     * @return
     */
    List<Integer> findMemberCountByMonths(List<String> monthList);

    /**
     * 查询性别名称 以及性别个数
     * @return
     */
    List<Map<String, Object>> getSexReport();
    /**
     * 查询年龄段名称 以及年龄段个数
     * @return
     */
    List<Map<String, Object>> getAgeReport();

    /**
     * 根据日期范围，获取每月会员报表
     * @param date
     * @return
     */
    Map findMembersByDate(List<Date> date) throws Exception;
}
