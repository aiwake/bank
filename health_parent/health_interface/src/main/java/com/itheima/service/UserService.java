package com.itheima.service;

import com.itheima.pojo.User;
import entity.PageResult;
import entity.QueryPageBean;

import java.util.List;
import java.util.Set;

/**
 * @Description:
 * @Author: yp
 */
public interface UserService {

    /**
     * 根据用户名查询
     *
     * @param username
     * @return
     */
    User findByUsername(String username);

    /**
     * 新增用户
     *
     * @param user
     * @param roleIds
     * @return
     */
    void add(User user, Integer[] roleIds);


    /**
     * 分页条件查询
     *
     * @param queryPageBean 封装的查询条件
     * @return 封装的返回结果
     */
    PageResult findPage(QueryPageBean queryPageBean) throws Exception;

    /**
     * 根据用户id查询用户
     *
     * @param id
     * @return
     */
    User findById(int id);

    /**
     * 更新用户
     *
     * @param user
     * @param roleIds
     */
    void edit(User user, Integer[] roleIds);

    /**
     * 根据id删除用户
     *
     * @param id
     */
    void delete(Integer id);

    /**
     * 修改密码
     *
     * @param user
     */
    void editPassword(User user);

    List changeUserName(String username, int id);


    List findByUsername2(String username);

    List getUserRole(String username);

    List changeUserRole(int id);
    /**
     * 将头像图片名称存入数据库
     * @param filename
     */
    void updateUserImgByFilename(String filename,String username);
}

