package com.itheima.service;

import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import entity.PageResult;
import entity.QueryPageBean;
import entity.Result;

public interface OrderListService {

    /**
     * 添加预约列表
     *
     * @param member
     * @param order
     */
    Result add(Member member, Order order);

    PageResult findPage(QueryPageBean queryPageBean) throws Exception;

    void delete(Integer id);
}
