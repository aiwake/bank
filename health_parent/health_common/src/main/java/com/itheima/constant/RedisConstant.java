package com.itheima.constant;

/**
 * @Description:
 * @Author: yp
 */
public class RedisConstant {
    //套餐图片所有图片名称(七牛云)
    public static final String SETMEAL_PIC_RESOURCES = "setmealPicResources";
    //套餐图片保存在数据库中的图片名称(数据库的)
    public static final String SETMEAL_PIC_DB_RESOURCES = "setmealPicDbResources";
    //移动端页面使用redis缓存套餐数据
    public static final String SETMEAL_CUR_KEY = "setmealCurKey";
    //移动端页面使用redis缓存套餐详情数据
    public static final String SETMEAL_BY_ID = "setmealById";
}
