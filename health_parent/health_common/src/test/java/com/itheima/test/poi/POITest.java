package com.itheima.test.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @Description: excel读写
 * @Author: yp
 */
public class POITest {

    @Test
    //读取excel
    public void fun01() throws Exception {
        // 1.创建工作簿对象
        XSSFWorkbook workbook = new XSSFWorkbook("G:/hello.xlsx");
        // 2.获得工作表对象
        XSSFSheet sheet = workbook.getSheetAt(0);
        // 3.遍历工作表对象 获得行对象
        for (Row row : sheet) {
            // 4.遍历行对象 获得列对象
            for (Cell cell : row) {
                // 5.获得数据
                System.out.println(cell.getStringCellValue());
            }
            System.out.println("-------------------");
        }
        // 6.关闭
        workbook.close();
    }

    @Test
    //写入
    public void fun02() throws Exception {
        //1.创建工作簿对象
        XSSFWorkbook workbook = new XSSFWorkbook();
        //2.创建工作表对象
        XSSFSheet sheet = workbook.createSheet("学生信息");
        //3.创建行对象
        XSSFRow row01 = sheet.createRow(0);
        //4.创建列(格子)对象, 设置内容
        row01.createCell(0).setCellValue("姓名");
        row01.createCell(1).setCellValue("性别");
        row01.createCell(2).setCellValue("地址");

        XSSFRow row02 = sheet.createRow(1);
        row02.createCell(0).setCellValue("张三");
        row02.createCell(1).setCellValue("男");
        row02.createCell(2).setCellValue("深圳");

        XSSFRow row03 = sheet.createRow(2);
        row03.createCell(0).setCellValue("李四");
        row03.createCell(1).setCellValue("女");
        row03.createCell(2).setCellValue("北京");

        //5.通过流写到磁盘
        FileOutputStream os = new FileOutputStream("G:/student.xlsx");
        workbook.write(os);

        os.close();
        workbook.close();


    }

}
