package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;
import com.itheima.utils.DateUtils;
import com.itheima.utils.POIUtils;
import entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: yp
 */
@RestController
@RequestMapping("/ordersetting")
public class OrderSettingController {

    @Reference
    private OrderSettingService orderSettingService;


    /**
     * 更新预约设置的人数
     * @return
     */
    @RequestMapping("/editNumberByDate")
    public Result editNumberByDate(@RequestBody OrderSetting orderSetting){
        try {
            orderSettingService.editNumberByDate(orderSetting);
            return new Result(true, MessageConstant.ORDERSETTING_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDERSETTING_FAIL);
        }
    }


    /**
     * 根据月份获得当前月的预约设置
     * @return
     */
    @RequestMapping("/getOrderSettingByMonth")
    public Result getOrderSettingByMonth(String date){
        try {
            //List<Map> list =  orderSettingService.getOrderSettingByMonth(date);
            List<OrderSetting> list =  orderSettingService.getOrderSettingByMonth02(date);
            return new Result(true, MessageConstant.GET_ORDERSETTING_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_ORDERSETTING_FAIL);
        }
    }

    @RequestMapping("/upload")
    public Result upload(MultipartFile excelFile){
        try {
            //1.使用POI解析文件 得到List<String[]> list
            List<String[]> list = POIUtils.readExcel(excelFile);

            //2.把List<String[]> list转成 List<OrderSetting> list
            List<OrderSetting> orderSettingList = new ArrayList<OrderSetting>();
            for (String[] array : list) {
                //字符串(2019.6.19)-转成date
                //OrderSetting orderSetting = new OrderSetting(new Date(array[0]), Integer.parseInt(array[1]));
                OrderSetting orderSetting = new OrderSetting(DateUtils.parseString2Date(array[0]), Integer.parseInt(array[1]));
                orderSettingList.add(orderSetting);
            }
            //3.调用业务 进行保存
            orderSettingService.add(orderSettingList);

            return new Result(true, MessageConstant.UPLOAD_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.UPLOAD_FAIL);
        }
    }


}
