package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.pojo.Permission;
import com.itheima.service.PermissionService;
import entity.PageResult;
import entity.QueryPageBean;
import entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:
 * @Author: yp
 */
@RestController
@RequestMapping("/permission")
public class PermissionController {
    @Reference
    private PermissionService permissionService;

    /**
     * 删除权限
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteById",method = RequestMethod.GET)
    public Result deleteById(Integer id) {
        try {
            permissionService.deleteByIdPermission(id);
            return new Result(true, MessageConstant.DELETE_PERMISSIONS_SUCCESS);
        }catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false,e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_PERMISSIONS_FAIL);
        }
    }


    /**
     * 更新权限
     * @param permission
     * @param roleIds
     * @return
     */
    @RequestMapping("/edit")
    public Result edit(@RequestBody Permission permission, Integer[] roleIds){
        try {
            permissionService.edit(permission,roleIds);
            return new Result(true, MessageConstant.EDIT_PERMISSION_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_PERMISSION_FAIL);
        }
    }


    /**
     * 根据权限的id查询出当前权限关联的角色的Id(查询中间表t_role_permission)
     * @return
     */
    @RequestMapping("/findRoleIdsByPerssionId")
    public Result findRoleIdsByPerssionId(Integer permissionId){
        try {
            List<Integer>  roleIds =  permissionService.findRoleIdsByPerssionId(permissionId);
            return new Result(true, MessageConstant.QUERY_ROLE_SUCCESS,roleIds);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_ROLE_FAIL);
        }
    }


    /**
     * 根据id查询权限
     * @param id
     * @return
     */
    @RequestMapping("/findById")
    public Result findById(Integer id){
        try {
            Permission permission =  permissionService.findById(id);
            return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS,permission);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_CHECKGROUP_FAIL);
        }
    }


    /**
     * 新增权限
     * @param permission
     * @param checkitemIds
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Permission permission, Integer[] checkitemIds) {
        try {
            permissionService.add(permission,checkitemIds);
            return new Result(true, MessageConstant.ADD_PERMISSION_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_PERMISSION_FAIL);
        }
    }


    /**
     * 权限管理分页查询
     * @param queryPageBean
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult =   permissionService.findPage(queryPageBean);
        return pageResult;
    }


}
