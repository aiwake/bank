package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Role;
import com.itheima.service.MenuService;
import entity.PageResult;
import entity.QueryPageBean;

import entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/menu")
public class MenuController {

    @Reference
    private MenuService menuService;


    /**
     * 分页查询
     * @param queryPageBean
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = menuService.findPage(queryPageBean);
        return pageResult;
    }


    /**
     * 新增页角色查询
     * @param
     * @return
     */
    @RequestMapping("/findAll")
    public Result findAll(){
        try {
            List<Role> list = menuService.findAll();
            return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_CHECKGROUP_FAIL);
        }
    }

    /**
     * 新增菜单角色组
     * @param
     * @param
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Menu menu,Integer[] checkRoleIds,Integer level) {
        try {
            menuService.add(menu,checkRoleIds,level);
            return new Result(true,"添加菜单成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "添加菜单失败");
        }
    }

    /**
     * 查询一级菜单列表
     * @return
     */
    @RequestMapping("/findParentMenuList")
    public Result findParentMenuList(){
        List<Menu> menus = menuService.findParentMenuList();
        return new Result(true,"父菜单查询成功",menus);
    }

    /**
     * 通过父菜单ID查询path
     * @param parentMenuId
     * @return
     */
    @RequestMapping("/findPathByParentMenuId")
    public Result findPathByParentMenuId(Integer parentMenuId){
        Menu pathByParentMenuId = menuService.findPathByParentMenuId(parentMenuId);
        return new Result(true,"查询成功",pathByParentMenuId);
    }

    /**
     * 查询所有关联角色ID
     * @param id
     * @return
     */
    @RequestMapping("/findCheckRoleIds")
    public Result findCheckRoleIds(Integer id){
        List<Integer> list = menuService.findCheckRoleIds(id);
        return new Result(true,"查询成功",list);
    }

    /**
     * 编辑
     * @param menu
     * @param checkRoleIds
     * @param level
     * @return
     */
    @RequestMapping("/edit")
    public Result edit(@RequestBody Menu menu,Integer[] checkRoleIds,Integer level){
        menuService.edit(menu,checkRoleIds,level);
        return new Result(true,"修改成功");
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(Integer id){
        menuService.del(id);
        return new Result(true,"删除成功");
    }
}

