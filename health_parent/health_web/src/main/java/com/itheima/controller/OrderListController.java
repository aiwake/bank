package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.service.OrderListService;
import com.itheima.utils.DateUtils;
import entity.PageResult;
import entity.QueryPageBean;
import entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/orderList")
public class OrderListController {

    @Reference
    private OrderListService orderListService;


    /**
     * 新增
     *
     * @param map
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Map map) {

        try {
            Member member = new Member((String) map.get("fileNumber"),
                    (String) map.get("name"),
                    (String) map.get("phoneNumber"));

            Date orderDate = DateUtils.parseString2Date((String) map.get("orderDate"));

            Order order = new Order(orderDate,(String) map.get("orderType"),(String) map.get("orderStatus"));

            return orderListService.add(member, order);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_ORDERLIST_FAIL);
        }
    }

    /**
     * 分页条件查询
     *
     * @param queryPageBean 封装的查询条件
     * @return 封装的返回结果
     */
    @RequestMapping(value = "/findPage", method = RequestMethod.POST)
    public Result findPage(@RequestBody QueryPageBean queryPageBean) {

        try {
            PageResult pageResult = orderListService.findPage(queryPageBean);
            return new Result(true, MessageConstant.QUERY_USER_SUCCESS, pageResult);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_USER_FAIL);
        }
    }

    /**
     * 根据id删除
     *
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(Integer id) {
        try {
            orderListService.delete(id);
            return new Result(true, MessageConstant.DELETE_USER_SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_USER_FAIL);
        }
    }

}
