package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.service.MemberService;
import com.itheima.service.ReportService;
import com.itheima.service.SetMealService;
import com.itheima.utils.DateUtils;
import entity.Result;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description:
 * @Author: yp
 */
@RestController
@RequestMapping("/report")
public class ReportController {


    @Reference
    private MemberService memberService;

    @Reference
    private SetMealService setMealService;

    @Reference
    private ReportService reportService;

    /**
     * 运营数据导出Excel
     * @return
     */
    @RequestMapping("/exportBusinessReport")
    public Result exportBusinessReport(HttpServletRequest request, HttpServletResponse response){
        try {
            //1.调用业务获得运营数据
            Map<String, Object> result = reportService.getBusinessReportData();

            String reportDate = (String) result.get("reportDate");
            Integer todayNewMember = (Integer) result.get("todayNewMember");
            Integer totalMember = (Integer) result.get("totalMember");
            Integer thisWeekNewMember = (Integer) result.get("thisWeekNewMember");
            Integer thisMonthNewMember = (Integer) result.get("thisMonthNewMember");
            Integer todayOrderNumber = (Integer) result.get("todayOrderNumber");
            Integer thisWeekOrderNumber = (Integer) result.get("thisWeekOrderNumber");
            Integer thisMonthOrderNumber = (Integer) result.get("thisMonthOrderNumber");
            Integer todayVisitsNumber = (Integer) result.get("todayVisitsNumber");
            Integer thisWeekVisitsNumber = (Integer) result.get("thisWeekVisitsNumber");
            Integer thisMonthVisitsNumber = (Integer) result.get("thisMonthVisitsNumber");
            List<Map> hotSetmeal = (List<Map>) result.get("hotSetmeal");

            //2.根据模版创建工作簿对象
            InputStream is = request.getSession().getServletContext().getResourceAsStream("template/report_template.xlsx");
            XSSFWorkbook workbook = new XSSFWorkbook(is);
            //3.获得工作表
            XSSFSheet sheet = workbook.getSheetAt(0);
            //4. 获得行, 列, 把数据进行填充
            XSSFRow row = sheet.getRow(2);
            row.getCell(5).setCellValue(reportDate);

            row = sheet.getRow(4);
            row.getCell(5).setCellValue(todayNewMember);//新增会员数（本日）
            row.getCell(7).setCellValue(totalMember);//总会员数

            row = sheet.getRow(5);
            row.getCell(5).setCellValue(thisWeekNewMember);//本周新增会员数
            row.getCell(7).setCellValue(thisMonthNewMember);//本月新增会员数

            row = sheet.getRow(7);
            row.getCell(5).setCellValue(todayOrderNumber);//今日预约数
            row.getCell(7).setCellValue(todayVisitsNumber);//今日到诊数

            row = sheet.getRow(8);
            row.getCell(5).setCellValue(thisWeekOrderNumber);//本周预约数
            row.getCell(7).setCellValue(thisWeekVisitsNumber);//本周到诊数

            row = sheet.getRow(9);
            row.getCell(5).setCellValue(thisMonthOrderNumber);//本月预约数
            row.getCell(7).setCellValue(thisMonthVisitsNumber);//本月到诊数

            int rowNum = 12;
            for(Map map : hotSetmeal){//热门套餐
                String name = (String) map.get("name");
                Long setmeal_count = (Long) map.get("setmeal_count");
                BigDecimal proportion = (BigDecimal) map.get("proportion");
                row = sheet.getRow(rowNum ++);
                row.getCell(4).setCellValue(name);//套餐名称
                row.getCell(5).setCellValue(setmeal_count);//预约数量
                row.getCell(6).setCellValue(proportion.doubleValue());//占比
            }


            //5, 通过文件流响应
            ServletOutputStream os = response.getOutputStream();
            //设置两个头(告诉浏览器文件的mime类型, 告诉浏览器下载)
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition","attachment;filename=report.xlsx");

            workbook.write(os);
            os.flush();
            os.close();
            is.close();
            workbook.close();



            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_BUSINESS_REPORT_FAIL);
        }
    }

    /**
     * 获得运营数据报表
     * @return
     */
    @RequestMapping("/getBusinessReportData")
    public Result getBusinessReportData(){
        try {
            Map<String,Object> map  = reportService.getBusinessReportData();
            return new Result(true, MessageConstant.GET_BUSINESS_REPORT_SUCCESS,map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_BUSINESS_REPORT_FAIL);
        }
    }

    /**
     * 预约套餐报表
     * @return
     */
    @RequestMapping("/getSetmealReport")
    public Result getSetmealReport(){
        try {
            //1.封装Data
            Map resultMap = new HashMap();
            //2.调用业务 获得 List<Map> setmealCount
            List<Map<String,Object>> setmealCount = setMealService.getSetmealReport();
            resultMap.put("setmealCount",setmealCount);

            //3.封装setmealNames ===>List<String> setmealNames
            List<String> setmealNames = new ArrayList<String>();
            for (Map<String, Object> map : setmealCount) {
                //取出name
                String name = (String) map.get("name");
                setmealNames.add(name);
            }
            resultMap.put("setmealNames",setmealNames);


            return new Result(true, MessageConstant.GET_SETMEAL_COUNT_REPORT_SUCCESS,resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_SETMEAL_COUNT_REPORT_FAIL);
        }
    }


    /**
     * 获得会员数量,日期报表
     * @return
     */
    @RequestMapping("/getMemberReport")
    public Result getMemberReport(){
        try {
            //1.创建Map
            Map resultMap = new HashMap();
            //2.封装months(过去一年的月份)
            List<String> monthList = new ArrayList<String>();
            Calendar calendar = Calendar.getInstance(); //当前的日期
            calendar.add(Calendar.MONTH,-12); //月份-12  2018-06
            for(int i =0;i<12;i++){
                calendar.add(Calendar.MONTH,1); //2018-07
                String month = DateUtils.parseDate2String(calendar.getTime(), "yyyy-MM");
                monthList.add(month);
            }
            resultMap.put("months",monthList);
            //3.调用service 获得 过去一年每个月份的会员总数量(累加的)
            List<Integer> memberCountList  = memberService.findMemberCountByMonths(monthList);
            //4.封装memberCount
            resultMap.put("memberCount",memberCountList);
            return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS,resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MEMBER_NUMBER_REPORT_FAIL);
        }
    }


    public static void main(String[] args) throws Exception {
        //封装months(过去一年的月份)  ['2018-10', '2018-11', '2018-12', '2019-01', '2018-02']
        List<String> monthList = new ArrayList<String>();

        Calendar calendar = Calendar.getInstance(); //当前的日期
        calendar.add(Calendar.MONTH,-12); //月份-12  2018-06

        for(int i =0;i<12;i++){
            calendar.add(Calendar.MONTH,1); //2018-07
            String month = DateUtils.parseDate2String(calendar.getTime(), "yyyy-MM");
            monthList.add(month);
        }

        System.out.println(monthList);

    }


    /**
     *查询性别名称 性别个数
     */
    @RequestMapping("/getSexReport")
    public Result getSexReport(){
        try {
            //1.封装Data
            Map resultMap = new HashMap();
            //2.调用业务 获得 List<Map> sexCount
            List<Map<String,Object>> sexCount = memberService.getSexReport();
            resultMap.put("sexCount",sexCount);

            //3.封装sexNames ===>List<Integer> setmealNames
            List<String> sexNames = new ArrayList<String>();
            for (Map<String, Object> map : sexCount) {
                //取出name
                String name = (String) map.get("name");
                sexNames.add(name);
            }
            resultMap.put("sexNames",sexNames);

            return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS,resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MEMBER_NUMBER_REPORT_FAIL);
        }
    }

    /**
     *查询年龄段名称  年龄段个数
     */
    @RequestMapping("/getAgeReport")
    public Result getAgeReport(){
        try {
            //1.封装Data
            Map resultMap = new HashMap();
            //2.调用业务 获得 List<Map> ageCount
            List<Map<String,Object>> ageCount = memberService.getAgeReport();
            resultMap.put("ageCount",ageCount);

            //3.封装ageNames ===>List<String> ageNames
            List<String> ageNames = new ArrayList<String>();
            for (Map<String, Object> map : ageCount) {
                //取出name
                String name = (String) map.get("name");
                ageNames.add(name);
            }
            resultMap.put("ageNames",ageNames);


            return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS,resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MEMBER_NUMBER_REPORT_FAIL);
        }
    }
    /**
     * 根据日期范围，获取每月会员报表
     *
     * @return
     */
    @RequestMapping("/getMemberReportDate")
    public Result getMemberReportDate(String[] date) {
        try {
//            Map resultMap = memberService.findMembersByDate(date);
            List<Date> list = new ArrayList<>();
            for (String s : date) {
//                Date date1 = new Date(s);
//                SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                Date parse = formatter.parse(s);

                Date d2 = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss z", Locale.US).parse(s);
//        Date endDate = DateUtils.parseDate2String(d2,"yyyy-MM");
                list.add(d2);
                System.out.println(d2);
            }
            Map membersByDate = memberService.findMembersByDate(list);
            return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS, membersByDate);
//            return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MEMBER_NUMBER_REPORT_FAIL);
        }
    }
}
