package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.service.RoleService;
import entity.PageResult;
import entity.QueryPageBean;
import entity.Result;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * create by Mr.huang
 * 666
 * 666
import com.itheima.pojo.Role;
import com.itheima.service.RoleService;
import entity.Result;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * @Description:
 * @Author: yp
 */
@RestController
@RequestMapping("/role")
public class RoleController {
    @Reference
    private RoleService roleService;
    @RequestMapping("/findById")
    public Result findById(Integer id){
        Role role = roleService.findById(id);
        return new Result(true,"查询成功",role);
    }
    /**
     * 分页查询
     * @param queryPageBean
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult =   roleService.findPage(queryPageBean);
        return pageResult;
    }

    /**
     * 添加角色
     * @param role
     * @param rolemenuIds
     * @param rolepermissionIds
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Role role, Integer[] rolemenuIds,Integer[] rolepermissionIds) {
        try {
            roleService.add(role,rolemenuIds,rolepermissionIds);
            return new Result(true, "添加角色成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"添加角色失败");
        }
    }

    /**
     * 编辑角色
     * @param role 角色信息
     * @param rolemenuIds 关联菜单ID
     * @param rolepermissionIds 关联权限
     * @return
     */
    @RequestMapping("/edit")
    public Result edit(@RequestBody Role role, Integer[] rolemenuIds,Integer[] rolepermissionIds){
        try {
            roleService.edit(role,rolemenuIds,rolepermissionIds);
            return new Result(true, "编辑成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"编辑失败");
        }
    }

    /**
     * 根据id删除
     * @return
     */
    @RequestMapping("/delete")
//    @PreAuthorize("hasAnyAuthority('CHECKITEM_DELETE')") //执行delete必须拥有CHECKITEM_DELETE权限
    public Result delete(Integer id){
        try {
            roleService.del(id);
            return new Result(true, "删除成功");
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除失败");
        }
    }

    /**
     * 查询关联菜单ID
     * @param id
     * @return
     */
    @RequestMapping("/findMenuIds")
    public Result findMenuIds(Integer id){
        List<Integer> menuById = roleService.findMenuById(id);
        if(menuById!=null && menuById.size()>0){
            return new Result(true,"查询成功",menuById);
        }
        return new Result(true,"查询失败");
    }

    /**
     * 查询关联权限ID
     * @param id
     * @return
     */
    @RequestMapping("/findPermissionIds")
    public Result findPermissionIds(Integer id){
        List<Integer> menuById = roleService.findPermissionById(id);
        if(menuById!=null && menuById.size()>0){
            return new Result(true,"查询成功",menuById);
        }
        return new Result(true,"查询失败");
    }

    /**
     * 查询所有权限
     * @return
     */
    @RequestMapping("/findPermissions")
    public Result findPermissions(){
        Set<Permission> permissions = roleService.findPermissions();
        return new Result(true,"查询成功",permissions);
    }

    /**
     * 查询所有菜单
     * @return
     */
    @RequestMapping("/findMenus")
    public Result findMenus(){
        LinkedHashSet<Menu> menus = roleService.findMenus();
        return new Result(true,"查询成功",menus);
    }



    /**
     * 查询所有角色
     *
     * @return
     */
    @RequestMapping("/findAll")
    public Result findAll() {
        try {
            List<Role> list = roleService.findAll();
            return new Result(true, MessageConstant.QUERY_ALLROLES_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_ALLROLES_FAIL);
        }
    }

}
