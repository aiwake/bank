package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisConstant;
import com.itheima.pojo.Setmeal;
import com.itheima.service.SetMealService;
import com.itheima.utils.QiniuUtils;
import com.itheima.utils.UploadUtils;
import entity.PageResult;
import entity.QueryPageBean;
import entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.JedisPool;

/**
 * @Description:
 * @Author: yp
 */
@RestController
@RequestMapping("/setmeal")
public class SetMealController {


    @Reference
    private SetMealService setMealService;


    @Autowired
    private JedisPool jedisPool;


    /**
     * 文件上传
     * @param imgFile
     * @return
     */
    @RequestMapping("/upload")
    public Result upload(MultipartFile imgFile){
        try {
            //1.获得文件名
            String filename = imgFile.getOriginalFilename();
            //2.把文件名替换成UUID文件名
            filename =  UploadUtils.getUUIDName(filename);
            //3.调用工具类进行上传
            QiniuUtils.upload2Qiniu(imgFile.getBytes(),filename);

            //把图片存到七牛云的set【redis】 sadd存set类型的, set存字符串类型
            jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES,filename);
            return new Result(true, MessageConstant.PIC_UPLOAD_SUCCESS,filename);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.PIC_UPLOAD_FAIL);
        }
    }

    /**
     * 新增套餐
     * @param setmeal
     * @param checkgroupIds
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Setmeal setmeal,Integer[] checkgroupIds){
        try {
            setMealService.add(setmeal,checkgroupIds);
            return new Result(true, MessageConstant.ADD_SETMEAL_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_SETMEAL_FAIL);
        }
    }

    /**
     * 分页条件查询
     *
     * @param queryPageBean 封装的查询条件
     * @return 封装的返回结果
     */

    @RequestMapping(value = "/findPage", method = RequestMethod.POST)
    public Result findPage(@RequestBody QueryPageBean queryPageBean) {

        try {
            PageResult pageResult = setMealService.findPage(queryPageBean);
            return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, pageResult);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_SETMEAL_FAIL);
        }
    }


}
