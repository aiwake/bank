package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;
import com.itheima.service.PermissionService;
import com.itheima.service.RoleService;
import com.itheima.service.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @Description:
 * @Author: yp
 */
@Component
public class SpringSecurityUserService implements UserDetailsService {
    @Reference
    private UserService userService;

    @Override
    //点击了登录, 这个方法会处理. username就是提交过来的用户名
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //1.根据用户名查询出User
        User user = userService.findByUsername(username);

        //2.判断User是否为null
        if (user == null) {
            return null;
        }

        //3.取出角色, 授权
        List<GrantedAuthority> grantedAuthorityList = new ArrayList<GrantedAuthority>();
        Set<Role> roles = user.getRoles();
        if(roles != null && roles.size()>0){
            for (Role role : roles) {
                //取出权限
                Set<Permission> permissions = role.getPermissions();
                if(permissions != null && permissions.size()>0){
                    for (Permission permission : permissions) {
                        grantedAuthorityList.add(new SimpleGrantedAuthority(permission.getKeyword()));
                    }
                }
            }
        }


        UserDetails userDetails = new org.springframework.security.core.userdetails.User(username,
                user.getPassword(),grantedAuthorityList);
        return userDetails;
    }


    /*@Reference
    private UserService userService;

    @Reference
    private RoleService roleService;

    @Reference
    private PermissionService permissionService;

    @Override
    //点击了登录, 这个方法会处理. username就是提交过来的用户名
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //1.根据用户名查询出User
        User user = userService.findByUsername(username);
        //2.判断User是否为null
        if (user == null) {
            return null;
        }
        //3.不为null,  根据user的id查询角色
        Integer userId = user.getId();
        Set<Role> roles =  roleService.findByUid(userId);
        //4.根据角色id查询出当前权限, 再封装进去(授权)
        List<GrantedAuthority> grantedAuthorityList = new ArrayList<GrantedAuthority>();
        if(roles!= null && roles.size()>0){
            for (Role role : roles) {
                //取出roleId
                Integer roleId = role.getId();
                Set<Permission> permissions = permissionService.findByRoleId(roleId);
                //遍历permissions
                if(permissions != null && permissions.size()>0){
                    for (Permission permission : permissions) {
                        grantedAuthorityList.add(new SimpleGrantedAuthority(permission.getKeyword()));
                    }
                }
            }
        }

        System.out.println(grantedAuthorityList.size());
        //5.授权
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(username,
                user.getPassword(),grantedAuthorityList);
        return userDetails;
    }*/
}
