package com.itheima.jobs;

/**
 * @version:1.8
 * @Auther:JJ89757
 * @Date:2019/7/26 11:14
 */



import com.alibaba.dubbo.config.annotation.Reference;

import com.itheima.service.OrderSettingService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;



/**
 * 定时清理历史数据
 */
@Component
public class ClearCacheJob {
  @Reference
   private OrderSettingService orderSettingService;
//5555
    /**
     * 注解方式不能使用L  @Scheduled( cron = "0 0 23 L * ?")
     */

    public void clearCache() {
        Logger logger = LoggerFactory.getLogger(ClearCacheJob.class);
        logger.debug("定时任务启动,清理开始");

        //获取Calendar
        Calendar calendar = Calendar.getInstance();
      // 设置Calendar月份数为下一个月
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
      // 设置Calendar日期为下一个月1号
        calendar.set(Calendar.DATE, 1);
     // 设置Calendar日期减1,为本月末
        calendar.add(Calendar.DATE, -1);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String lastDay = format.format(calendar.getTime());
      //定时清理
   orderSettingService.deleteCache(lastDay);
        logger.debug("清理结束");
    }
}


