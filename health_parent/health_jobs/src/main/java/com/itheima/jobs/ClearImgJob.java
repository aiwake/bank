package com.itheima.jobs;

import com.itheima.constant.RedisConstant;
import com.itheima.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.JedisPool;

import java.util.Set;

/**
 * @Description:
 * @Author: yp
 */
public class ClearImgJob {

    @Autowired
    private JedisPool jedisPool;

    public void clearImg() {
        //1.使用Jedis求出两个Set的差值
        Set<String> pics = jedisPool.getResource().sdiff(RedisConstant.SETMEAL_PIC_RESOURCES, RedisConstant.SETMEAL_PIC_DB_RESOURCES);
        //2.遍历差值
        for (String pic : pics) {
            //3.调用QiniuUtils删除图片,
            QiniuUtils.deleteFileFromQiniu(pic);
            //4.redis里面的图片也需要删除(七牛云Set)
            jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_RESOURCES,pic);

        }

    }
}
