package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.itheima.dao.MenuDao;


import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Role;
import entity.PageResult;
import entity.QueryPageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = MenuService.class)
@Transactional
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuDao menuDao;


    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //1.调用分页插件的方法
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        //2.调用Dao, 封装PageResult

        Page<Menu> page =menuDao.findConditions( queryPageBean.getQueryString());
        PageResult pageResult = new PageResult(page.getTotal(), page.getResult());
        return pageResult;
    }

    @Override
    public List<Role> findAll() {
        return menuDao.findAll();
    }


    /**
     * 新增菜单角色
     * @param
     * @param
     */
    @Override
    public void add(Menu menu, Integer[] checkRoleIds,Integer level) {

            //1.调用Dao,保存Menu【返回当前保存的id】
            if(level.equals(1)){
                menuDao.addFirst(menu);
            }else if(level.equals(2)){
                menuDao.addSecond(menu);
            }
            //
            Integer menuId = menu.getId();
            setMenuIdAndCheckRoleIds(checkRoleIds,menuId);
        }

    @Override
    public List<Integer> findCheckRoleIds(Integer id) {
        List<Integer> checkRoleIds = menuDao.findCheckRoleIds(id);
        return checkRoleIds;
    }

    @Override
    public void edit(Menu menu, Integer[] checkRoleIds, Integer level) {
        //先修改menu表数据
        if(level.equals(1)){
            //更新一级菜单
            menuDao.editFirst(menu);
        }else if(level.equals(2)){
            //更新二级菜单
            menuDao.editSecond(menu);
        }
        //删除中间表数据
        menuDao.deleteMenuAdnRole(menu.getId());
        //增加中间表数据
        setRoleAndMenu(menu.getId(),checkRoleIds);
    }

    @Override
    public void del(Integer id) {
        //先删除中间表数据
        menuDao.deleteMenuAdnRole(id);
        //再menu
        menuDao.delById(id);
    }

    private void setRoleAndMenu(Integer id, Integer[] checkRoleIds) {
        if(checkRoleIds!=null){
            for (Integer roleId : checkRoleIds) {
                Map map = new HashMap();
                map.put("roleId",roleId);
                map.put("menuId",id);
                menuDao.setRoleAndMenu(map);
            }
        }
    }

    @Override
    public List<Menu> findParentMenuList() {
        List<Menu> menus = menuDao.findParentMenuList();
        return menus;
    }

    @Override
    public Menu findPathByParentMenuId(Integer id) {
        Menu menu = menuDao.findPathByParentMenuId(id);
        return menu;
    }




/*    @Override
    public void add(CheckGroup checkGroup, Integer[] checkitemIds) {
        //1.调用Dao,保存CheckGroup【返回当前保存的id】
        checkGroupDao.add(checkGroup);
        //2.调用Dao, 把选择的检查项和检查组的id保存到t_checkgroup_checkitem
        Integer checkGroupId = checkGroup.getId();
        setCheckGroupAndCheckItem(checkGroupId,checkitemIds);
    }*/

    private void setMenuIdAndCheckRoleIds(Integer[] checkRoleIds, Integer menuId) {
        if(checkRoleIds != null){
            for (Integer RoleId : checkRoleIds) {
                Map map = new HashMap();
                map.put("menuId",menuId);
                map.put("checkRoleIds",RoleId);
                menuDao.setMenuIdAndCheckRoleIds(map);
            }
        }


    }

}


