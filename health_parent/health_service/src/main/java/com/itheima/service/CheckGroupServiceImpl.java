package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckGroupDao;
import com.itheima.dao.SetMealDao;
import com.itheima.pojo.CheckGroup;
import entity.PageResult;
import entity.QueryPageBean;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: yp
 */
@Service(interfaceClass = CheckGroupService.class)
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {

    @Autowired
    private CheckGroupDao checkGroupDao;
    @Autowired
    private SetMealDao setMealDao;

    /**
     * 新增检查组
     * @param checkGroup
     * @param checkitemIds
     */
    @Override
    public void add(CheckGroup checkGroup, Integer[] checkitemIds) {
        //1.调用Dao,保存CheckGroup【返回当前保存的id】
        checkGroupDao.add(checkGroup);
        //2.调用Dao, 把选择的检查项和检查组的id保存到t_checkgroup_checkitem
        Integer checkGroupId = checkGroup.getId();
        setCheckGroupAndCheckItem(checkGroupId,checkitemIds);
    }

    /**
     * 分页查询
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //1.调用分页插件的方法
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        //2.调用Dao, 封装PageResult



        Page<CheckGroup> page =  checkGroupDao.findConditions( queryPageBean.getQueryString());
        PageResult pageResult = new PageResult(page.getTotal(), page.getResult());
        return pageResult;
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @Override
    public CheckGroup findById(Integer id) {
        return checkGroupDao.findById(id);
    }

    /**
     * 根据checkGroupId查询检查项的ids
     *
     * @param checkGroupId
     * @return
     */
    @Override
    public List<Integer> findCheckItemIdsByCheckGroupId(Integer checkGroupId) {
        return checkGroupDao.findCheckItemIdsByCheckGroupId(checkGroupId);

    }

    /**
     * 修改检查组
     * @param checkGroup
     * @param checkitemIds
     */
    @Override
    public void edit(CheckGroup checkGroup, Integer[] checkitemIds) {
        //1.调用dao 更新CheckGroup(t_checkgroup)
        checkGroupDao.edit(checkGroup);
        //2.调用dao 先把CheckGroup关联的检查项给删除(t_checkgroup_checkitem)
        checkGroupDao.deleteAssociation(checkGroup.getId());
        //3.调用dao 把更新提交过来的checkitemIds 再进行关联
        setCheckGroupAndCheckItem(checkGroup.getId(),checkitemIds);

    }

    /**
     * 查询所有的检查组
     *
     * @return
     */
    @Override
    public List<CheckGroup> findAll() {
        return checkGroupDao.findAll();
    }
    /**
     * 删除检查组
     */

    @Override
    public void delete(Integer id) {

        //先删除关联的中间表
        List<Integer> list = checkGroupDao.findCheckItemIdsByCheckGroupId(id);
        List<CheckGroup> checkGroups = setMealDao.findByCheckGroupId(id);
        if (list.size() > 0||checkGroups.size()>0) {
            //有关联的检查项，先删除中间表
            checkGroupDao.deleteCheckItem(id);
            setMealDao.deleteById(id);
        }

        //3.没有关联,调用Dao 删除
        checkGroupDao.deleteById(id);

    }

    //向t_checkgroup_checkitem保存数据
    private void setCheckGroupAndCheckItem(Integer checkGroupId, Integer[] checkitemIds) {
        if(checkitemIds != null){
            for (Integer checkItemId : checkitemIds) {
                Map map = new HashMap();
                map.put("checkGroupId",checkGroupId);
                map.put("checkItemId",checkItemId);
                checkGroupDao.setCheckGroupAndCheckItem(map);
            }
        }
    }
}
