package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.OrderSettingDao;
import com.itheima.pojo.OrderSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: yp
 */
@Service(interfaceClass = OrderSettingService.class)
@Transactional
public class OrderSettingServiceImpl implements OrderSettingService {

    @Autowired
    private OrderSettingDao orderSettingDao;

    /**
     * 添加预约设置
     *
     * @param orderSettingList
     */
    @Override
    public void add(List<OrderSetting> orderSettingList) {
        //1.遍历List<OrderSetting> list
        for (OrderSetting orderSetting : orderSettingList) {
            //2.判断当前的日期之前是否设置过(就是根据日期查询t_ordersetting)
            long count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
            if (count > 0) {
                //2.1 如果设置过, 更新数量(根据日期来更新number)
                orderSettingDao.editNumberByOrderDate(orderSetting);
            } else {
                //2.2 没有设置过, 保存
                orderSettingDao.add(orderSetting);
            }
        }
    }

    /**
     * 根据月份获得当前月的预约设置
     *
     * @param date
     * @return
     */
    @Override
    public List<Map> getOrderSettingByMonth(String date) { //2019-06
        //orderDate BETWEEN '2019-06-01' AND '2019-06-31'
        //1.拼接参数
        String dateBegin = date + "-01";
        String dateEnd = date + "-31";
        Map paramsMap = new HashMap();
        paramsMap.put("dateBegin", dateBegin);
        paramsMap.put("dateEnd", dateEnd);
        //2.调用Dao
        List<OrderSetting> list = orderSettingDao.getOrderSettingByMonth(paramsMap);
        //3.把得List<OrderSetting> list转成List<Map> list
        List<Map> mapList = new ArrayList<Map>();
        if (list != null) {
            for (OrderSetting orderSetting : list) {
                //一个orderSetting对象转成一个Map  {date: 1, number: 120, reservations: 1}
                Map map = new HashMap();
                map.put("date", orderSetting.getOrderDate().getDate()); //获得日
                map.put("number", orderSetting.getNumber()); //获得最大预约数量
                map.put("reservations", orderSetting.getReservations()); //获得已经预约数量

                mapList.add(map);
            }
        }
        return mapList;
    }

    /**
     * 根据月份获得当前月的预约设置
     *
     * @param date
     * @return
     */
    @Override
    public List<OrderSetting> getOrderSettingByMonth02(String date) {  //2019-06
        //1.拼接参数
        String dateBegin = date + "-01";
        String dateEnd = date + "-31";

        Map paramsMap = new HashMap();
        paramsMap.put("dateBegin", dateBegin);
        paramsMap.put("dateEnd", dateEnd);
        //2.调用Dao
        List<OrderSetting> list = orderSettingDao.getOrderSettingByMonth(paramsMap);
        return list;
    }

    /**
     * 更新预约设置的人数
     *
     * @param orderSetting
     */
    @Override
    public void editNumberByDate(OrderSetting orderSetting) {
        //1.判断当前的日期是否设置过
        long count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
        if (count > 0){
            // 2.有设置过, 更新(根据orderDate更新Number)
            orderSettingDao.editNumberByOrderDate(orderSetting);
        }else{
            //3. 没有设置过, 新增
            orderSettingDao.add(orderSetting);
        }




    }
    /**
     * 定时清理数据
     *
     * @param lastDay
     */
    @Override
    public void deleteCache(String lastDay) {
       orderSettingDao.deleteCache(lastDay) ;
    }
}
