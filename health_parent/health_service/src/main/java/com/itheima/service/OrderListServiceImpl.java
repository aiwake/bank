package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constant.MessageConstant;
import com.itheima.dao.MemberDao;
import com.itheima.dao.OrderDao;
import com.itheima.dao.OrderSettingDao;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.pojo.OrderSetting;
import com.itheima.pojo.User;
import com.itheima.utils.DateUtils;
import com.sun.xml.internal.rngom.digested.DUnaryPattern;
import entity.PageResult;
import entity.QueryPageBean;
import entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = OrderListService.class)
@Transactional
public class OrderListServiceImpl implements OrderListService {

    @Autowired
    private MemberDao memberDao;

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private OrderSettingDao orderSettingDao;

    /**
     * 添加预约列表
     *
     * @param member
     * @param order
     */
    @Override
    public Result add(Member member, Order order) {

        //先查此会员是否存在
        Member memberDB = memberDao.findByTelephone(member.getPhoneNumber());

        OrderSetting orderSetting = orderSettingDao.findByOrderDate(order.getOrderDate());
        if (orderSetting == null) {
            //当前日期不能预约
            return new Result(false, MessageConstant.SELECTED_DATE_CANNOT_ORDER);
        }

        // 判断当前日期是否预约已满(判断reservations是否==number)
        if (orderSetting.getReservations() == orderSetting.getNumber()) {
            //预约已满
            return new Result(false, MessageConstant.ORDER_FULL);
        }

        if (memberDB != null) {

            //如果是会员(能够查询出来), 防止重复预约(根据member_id,orderDate,setmeal_id查询t_order)
            List<Order> list = orderDao.findByCondition(order);
            if (list != null && list.size() > 0) {
                //已经预约过了
                return new Result(false, MessageConstant.HAS_ORDERED);
            }

        } else {
            memberDao.add(member);
        }

        // 4. 进行预约
        // 4.1向t_order表插入一条记录
        order.setMemberId(member.getId());
        orderDao.add(order);
        // 4.2 t_ordersetting表里面预约reservations的人数+1
        orderSetting.setReservations(orderSetting.getReservations() + 1);
        orderSettingDao.editReservationsByOrderDate(orderSetting);

        return new Result(true,MessageConstant.ADD_ORDERLIST_SUCCESS);
    }

    /**
     * 分页条件查询
     *
     * @param queryPageBean 封装的查询条件
     * @return 分页结果封装对象
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) throws Exception {

        //调用Mybatis分页插件方法(自动帮我们做分页)
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());

        //调用Dao, 获得Page对象(page对象是分页插件自带的, 包含List, total...)
        Page<Map> page = memberDao.findPage(queryPageBean.getQueryString());

        List<Map> result = page.getResult();
        for (Map map : result) {
            Date date = (Date) map.get("orderDate");
            String orderDate = DateUtils.parseDate2String(date);
            map.put("orderDate", orderDate);
        }


        return new PageResult(page.getTotal(), page.getResult());
    }


    /**
     * 根据id删除用户
     *
     * @param id
     */
    @Override
    public void delete(Integer id) {

        //通过id删除order
        orderDao.deleteById(id);
    }
}
