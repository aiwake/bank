package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.MemberDao;
import com.itheima.pojo.Member;
import com.itheima.utils.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @Description:
 * @Author: yp
 */
@Service(interfaceClass = MemberService.class)
@Transactional
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberDao memberDao;

    /**
     * 根据手机号码查询会员
     * @param telephone
     * @return
     */
    @Override
    public Member findByTelephone(String telephone) {
        return memberDao.findByTelephone(telephone);
    }

    /**
     * 注册
     *
     * @param member
     */
    @Override
    public void register(Member member) {
        //建议把密码加密
        if(member.getPassword() != null && !"".equals(member.getPassword())){
            member.setPassword(MD5Utils.md5(member.getPassword()));
        }

        memberDao.add(member);
    }

    /**
     * 统计每个月分的会员的总数(累加)
     *
     * @param monthList
     * @return
     */
    @Override
    public List<Integer> findMemberCountByMonths(List<String> monthList) {
        List<Integer> memberCountList = new ArrayList<Integer>();

        //遍历
        for (String date : monthList) {
            //2019-06
            date = date+"-31";
            //查询 SELECT count(*) FROM t_member WHERE regTime <= '2019-06-31'
            Integer memberCount = memberDao.findMemberCountBeforeDate(date);

            memberCountList.add(memberCount);

        }
        return memberCountList;
    }

    @Override
    public List<Map<String, Object>> getSexReport() {
        List<Map<String, Object>> sexReport = memberDao.getSexReport();
        return sexReport;
    }

    /**
     * 查询年龄段名称 以及年龄段个数
     * @return
     */
    @Override
    public List<Map<String, Object>> getAgeReport() {
        List<Map<String, Object>> ageRepoet = memberDao.getAgeReport();
        return ageRepoet;
    }

    /**
     * 根据日期范围，获取每月会员报表
     *
     * @param date
     * @return
     */
    @Override
    public Map findMembersByDate(List<Date> date) throws Exception{
        //date=Tue Jan 01 2019 00:00:00 GMT 0800 (中国标准时间),Mon Jul 01 2019 00:00:00 GMT 0800 (中国标准时间)
        //截取开始时间，并转换格式
//        int indexOf = date.indexOf("(");
//        String startDate = date.substring(0, indexOf-1);
//        Date d = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss z", Locale.US).parse(startDate);
//        startDate = DateUtils.parseDate2String(d,"yyyy-MM");
//        String start = DateUtils.parseDate2String(d,"MM");
//        System.out.println("~~~~~~~~~"+start);


        //截取结束时间，并转换格式
//        int lastIndexOf = date.lastIndexOf("(");
//        String endDate = date.substring(0, lastIndexOf-1);
//        Date d2 = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss z", Locale.US).parse(endDate);
//        endDate = DateUtils.parseDate2String(d2,"yyyy-MM");


//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");//格式化为年月

//        Calendar min = Calendar.getInstance();
//        Calendar max = Calendar.getInstance();
        Date start  = date.get(0);
        Date end  = date.get(1);
        Map<String,Date> map = new HashMap<>();
        map.put("start",start);
        map.put("end",end);
        List<Map<String,Object>> memberLists = memberDao.findMemberByTime(map);
//        System.out.println(memberLists+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        List memberCount = new ArrayList<>();
        List months = new ArrayList<>();
        for (Map<String, Object> memberList : memberLists) {
            memberCount.add(memberList.get("memberCount"));
            months.add(memberList.get("months"));
        }
        Map resultMap = new HashMap();
        resultMap.put("memberCount",memberCount);
        resultMap.put("months",months);
        return resultMap;
    }
}
