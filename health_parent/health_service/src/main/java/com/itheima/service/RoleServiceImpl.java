package com.itheima.service;


import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.RoleDao;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import entity.PageResult;
import entity.QueryPageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @Description:
 * @Author: yp
 */
@Service(interfaceClass = RoleService.class)
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    /**
     * 根据用户id查询角色
     *
     * @param userId
     * @return
     */
    @Override
    public Set<Role> findByUid(Integer userId) {
        return roleDao.findByUid(userId);
    }

    /**
     * 分页查询角色
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //1.调用分页插件的方法
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        //2.调用Dao, 封装PageResult
        Page<Role> page =  roleDao.findConditions( queryPageBean.getQueryString());
        PageResult pageResult = new PageResult(page.getTotal(), page.getResult());
        return pageResult;
    }

    /**
     * 根据角色Id删除角色
     *  四张表——角色权限mid+角色菜单mid+角色用户mid+角色
     * @param id
     */
    @Override
    public void del(Integer id) {
        //1.判断角色是否被用户使用
//        long count = roleDao.findCountByRoleId(id);
//        if(count>0){
//            //角色已经被使用不能删除
//            throw new RuntimeException("此角色已被用户使用，不能删除");
//        }
        //没有引用__删除三张表的数据
        roleDao.deleteUserAndRole(id);
        roleDao.deleteRoleAndMenu(id);
        roleDao.deleteRoleAndPermission(id);
        roleDao.deleteById(id);
        //2.判断角色是否与权限关联

        //3.判断角色是否与菜单关联
    }

    /**
     * 添加角色
     * @param role 角色信息
     * @param rolemenuIds 关联菜单
     * @param rolepermissionIds 关联权限
     */
    @Override
    public void add(Role role, Integer[] rolemenuIds, Integer[] rolepermissionIds) {
        roleDao.add(role);
        Integer roleId = role.getId();
        setRoleAndMenu(roleId,rolemenuIds);
        setRoleAndPermission(roleId,rolepermissionIds);
    }
    //向t_role_permission保存信息
    private void setRoleAndPermission(Integer roleId, Integer[] rolepermissionIds) {
        if(rolepermissionIds!=null){
            for (Integer rolepermissionId : rolepermissionIds) {
                Map map = new HashMap();
                map.put("roleId",roleId);
                map.put("permissionId",rolepermissionId);
                roleDao.setRoleAndPermission(map);
            }
        }
    }
    //向t_role_menu保存信息
    private void setRoleAndMenu(Integer roleId, Integer[] rolemenuIds) {
        if(rolemenuIds!=null){
            for (Integer rolemenuId : rolemenuIds) {
                Map map = new HashMap();
                map.put("roleId",roleId);
                map.put("menuId",rolemenuId);
                roleDao.setRoleAndMenu(map);
            }
        }
    }

    /**
     * 编辑角色
     * @param role 角色信息
     * @param rolemenuIds 关联菜单ID
     * @param rolepermissionIds 关联权限
     * @return
     */
    @Override
    public void edit(Role role, Integer[] rolemenuIds, Integer[] rolepermissionIds) {
        //更新role表
        roleDao.edit(role);
        //先删除中间表数据
        roleDao.deleteRoleAndMenu(role.getId());
        //新增中间表数据
        setRoleAndMenu(role.getId(),rolemenuIds);
        //先删除中间表数据
        roleDao.deleteRoleAndPermission(role.getId());
        //再新增数据
        setRoleAndPermission(role.getId(),rolepermissionIds);
    }

    /**
     * 根据ID查询权限
     * @param id
     * @return
     */
    @Override
    public List<Integer> findPermissionById(Integer id) {
        List<Integer> lists = roleDao.findPermissionIds(id);
        return lists;
    }

    /**
     * 根据ID查询菜单
     * @param id
     * @return
     */
    @Override
    public List<Integer> findMenuById(Integer id) {
        List<Integer> lists = roleDao.findMenuIds(id);
        return lists;
    }

    @Override
    public Set<Permission> findPermissions() {
        Set<Permission> permissions = roleDao.findPermissions();
        return permissions;
    }

    @Override
    public LinkedHashSet<Menu> findMenus() {
        LinkedHashSet<Menu> menus = roleDao.findMenus();
        return menus;
    }

    @Override
    public Role findById(Integer id) {
        Role role = roleDao.findById(id);
        return role;
    }



    /**
     * 查询所有角色
     *
     * @return
     */
    @Override
    public List<Role> findAll() {

        return roleDao.findAll();
    }

    /**
     * 根据userId查询角色的roleIds
     *
     * @param userId
     * @return
     */
    @Override
    public List<Integer> findRoleIdsByUserId(int userId) {

        return roleDao.findRoleIdsByUserId(userId);
    }

}
