package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.constant.MessageConstant;
import com.itheima.dao.MemberDao;
import com.itheima.dao.OrderDao;
import com.itheima.dao.OrderSettingDao;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.pojo.OrderSetting;
import com.itheima.utils.DateUtils;
import entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: yp
 */
@Service(interfaceClass = OrderService.class)
@Transactional
public class OrderServiceImpl implements OrderService {


    @Autowired
    private OrderSettingDao orderSettingDao;

    @Autowired
    private MemberDao memberDao;

    @Autowired
    private OrderDao orderDao;

    /**
     * 预约
     *
     * @param map
     * @return { "setmealId": "12", "sex": "1", "name": "张三", "telephone": "15374518821",
     * "validateCode": "127890", "idCard": "512501197203035172",
     * "orderDate": "2019-07-22" }
     */
    @Override
    public Result submit(Map map) throws Exception {

        // 1.  判断当前的日期是否可以预约(根据orderDate查询t_ordersetting, 能查询出来可以预约;查询不出来,不能预约 的)
        String orderDateStr = (String) map.get("orderDate");
        Date orderDate = DateUtils.parseString2Date(orderDateStr);
        OrderSetting orderSetting = orderSettingDao.findByOrderDate(orderDate);
        if (orderSetting == null) {
            //当前日期不能预约
            return new Result(false, MessageConstant.SELECTED_DATE_CANNOT_ORDER);
        }

        // 2.  判断当前日期是否预约已满(判断reservations是否==number)
        if (orderSetting.getReservations() == orderSetting.getNumber()) {
            //预约已满
            return new Result(false, MessageConstant.ORDER_FULL);
        }

        // 3.  判断是否是会员(根据手机号码查询t_member)
        String telephone = (String) map.get("telephone");
        String setmealId = (String) map.get("setmealId");
        Member member = memberDao.findByTelephone(telephone);
        if(member != null){
            // 3.1如果是会员(能够查询出来), 防止重复预约(根据member_id,orderDate,setmeal_id查询t_order)
            Order order = new Order();
            order.setMemberId(member.getId());
            order.setOrderDate(orderDate);
            order.setSetmealId(Integer.parseInt(setmealId));
            List<Order> list = orderDao.findByCondition(order);
            if (list != null && list.size()>0){
                //已经预约过了
                return new Result(false, MessageConstant.HAS_ORDERED);
            }
        }else{
            // 3.2 如果不是会员(不能够查询出来),自动注册为会员(直接向t_member插入一条记录)
            member = new Member();
            member.setName((String) map.get("name"));
            member.setPhoneNumber(telephone);
            member.setIdCard((String) map.get("idCard"));
            member.setSex((String) map.get("sex"));
            member.setRegTime(new Date());
            memberDao.add(member);
        }

        // 4. 进行预约
        // 4.1向t_order表插入一条记录
        Order order = new Order(member.getId(),orderDate,Order.ORDERTYPE_WEIXIN,Order.ORDERSTATUS_NO,Integer.parseInt(setmealId));
        order.setBookDate(new Date());
        orderDao.add(order);
        // 4.2 t_ordersetting表里面预约reservations的人数+1
        orderSetting.setReservations(orderSetting.getReservations()+1);
        orderSettingDao.editReservationsByOrderDate(orderSetting);

        return new Result(true,MessageConstant.ORDER_SUCCESS,order);
    }

    /**
     * 根据id查询预约信息
     * @param id
     * @return
     */
    @Override
    public Map findById(Integer id) {
        Map map = orderDao.findById4Detail(id);
        return map;
    }
}
