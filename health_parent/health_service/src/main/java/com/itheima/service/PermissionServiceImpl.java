package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.PermissionDao;
import com.itheima.pojo.Permission;
import entity.PageResult;
import entity.QueryPageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Description:
 * @Author: yp
 */
@Service(interfaceClass = PermissionService.class)
@Transactional
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionDao permissionDao;

    /**
     * 根据角色id查询权限
     * @param roleId
     * @return
     */
    @Override
    public Set<Permission> findByRoleId(Integer roleId) {
        return permissionDao.findByRoleId(roleId);
    }

    /**
     * 权限管理分页查询
     *
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //1.调用分页插件的方法
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        //2.调用Dao, 封装PageResult
        Page<Permission> page =  permissionDao.findConditions( queryPageBean.getQueryString());
        PageResult pageResult = new PageResult(page.getTotal(), page.getResult());
        return pageResult;
    }

    /**
     * 新增权限
     *
     * @param permission
     * @param checkitemIds
     */
    @Override
    public void add(Permission permission, Integer[] checkitemIds) {
        //1.调用Dao,保存Role【返回当前保存的id】
        permissionDao.add(permission);
        //2.调用Dao, 把选择的检查项和检查组的id保存到t_checkgroup_checkitem
        Integer permissionId = permission.getId();
        setPermissionAndRole(permissionId,checkitemIds);
    }

    /**
     * 根据id查询权限
     *
     * @param id
     * @return
     */
    @Override
    public Permission findById(Integer id) {
        return permissionDao.findById(id);
    }

    /**
     * 根据权限的id查询出当前权限关联的角色的Id(查询中间表t_role_permission)
     *
     * @param permissionId
     * @return
     */
    @Override
    public List<Integer> findRoleIdsByPerssionId(Integer permissionId) {
        return permissionDao.findRoleIdsByPerssionId(permissionId);
    }

    /**
     * 更新权限
     *
     * @param permission
     * @param roleIds
     */
    @Override
    public void edit(Permission permission, Integer[] roleIds) {
        //1.调用dao 更新t_permissiont
        permissionDao.edit(permission);
        //2.调用dao 先把t_permissiont关联的检查项给删除(t_role_permission)
        permissionDao.deleteAssociation(permission.getId());
        //3.调用dao 把更新提交过来的checkitemIds 再进行关联
        setPermissionAndRole(permission.getId(),roleIds);
    }

    /**
     * 删除权限
     *
     * @param id
     */
    @Override
    public void deleteByIdPermission(Integer id) {
        //先删中间表
        permissionDao.deleteAssociation(id);
        permissionDao.deleteByIdPermission(id);
    }

    //向t_role_permission保存数据
    private void setPermissionAndRole(Integer permissionId, Integer[] checkitemIds) {
        if(checkitemIds != null){
            for (Integer checkItemId : checkitemIds) {
                Map map = new HashMap();
                map.put("permissionId",permissionId);
                map.put("checkItemId",checkItemId);
                permissionDao.setPermissionAndRole(map);
            }
        }
    }

}
