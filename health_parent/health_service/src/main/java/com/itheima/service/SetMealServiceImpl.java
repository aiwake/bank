package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constant.RedisConstant;
import com.itheima.dao.SetMealDao;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Setmeal;
import entity.PageResult;
import entity.QueryPageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: yp
 */
@Service(interfaceClass = SetMealService.class)
@Transactional
public class SetMealServiceImpl implements SetMealService {

    @Autowired
    private SetMealDao setMealDao;

    @Autowired
    private JedisPool jedisPool;

    /**
     * 新增套餐
     * @param setmeal
     * @param checkgroupIds
     */
    @Override
    public void add(Setmeal setmeal, Integer[] checkgroupIds) {
        //向t_setmeal插入一条记录
        setMealDao.add(setmeal);
        // 向t_setmeal_checkgroup插入多条记录
        Integer setmealId = setmeal.getId();
        setSetmealAndCheckGroup(setmealId,checkgroupIds);
        //把图片存到数据库的set【redis】
        jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_DB_RESOURCES,setmeal.getImg());
    }

    /**
     * 查询所有的套餐
     *
     * @return
     */
    @Override
    public List<Setmeal> getSetmeal() {
        List<Setmeal> setmealList = null;
        try {
            //1.从redis里面获取所有套餐数据
            String data = jedisPool.getResource().get(RedisConstant.SETMEAL_CUR_KEY);
            if (data != null) {
                setmealList = JSON.parseArray(data, Setmeal.class);
                System.out.println("Redis里面有数据，从Redis里面获取");
            }
            if (data == null) {
                //2.reidsl里面没有数据，从mysql里面获取
                setmealList = setMealDao.getSetmeal();
                String toJSONsetmealList = JSON.toJSONString(setmealList);
                //2.1存储到redis
                jedisPool.getResource().set(RedisConstant.SETMEAL_CUR_KEY,toJSONsetmealList);
                System.out.println("Redis里面没有数据，从mysql里面获取，并存入redis");
            }
        } catch (Exception e) {
            e.printStackTrace();
            //2.reidsl里面没有数据，从mysql里面获取
            setmealList = setMealDao.getSetmeal();
            System.out.println("Redis里面没有数据，从mysql里面获取，并存入redis");
        }finally {
            return setmealList;
        }

    }
    /**
     * 套餐详情(1.先直接查询套餐的基本信息 2.关联查询出检查组的信息List 3.关联查询出检查项的信息List)
     * @param id
     * @param type
     * @return
     */
    @Override
    public Setmeal findById(Integer id, Integer type) {
        Setmeal setmeal = setMealDao.findById(id);

        if(type == 1){
            //不懒(使用一下)
            List<CheckGroup> checkGroups = setmeal.getCheckGroups();
            System.out.println(checkGroups.size());
            for (CheckGroup checkGroup : checkGroups) {
                List<CheckItem> checkItems = checkGroup.getCheckItems();
                System.out.println(checkItems.size());
            }
        }

        System.out.println(setmeal);
        //取出setmeal的id, 调用CheckGroupDao 查询出List<CheckGroup> list 封装
        //遍历List<CheckGroup> list, 取出每一个CheckGroup的id, 调用CheckItemDao 查出 List<CheckItem> checkItems;
        return setmeal;
    }

    @Override
    public List<Map<String, Object>> getSetmealReport() {
        return setMealDao.getSetmealReport();
    }

    private void setSetmealAndCheckGroup(Integer setmealId, Integer[] checkgroupIds) {
        if(checkgroupIds != null){
            for (Integer checkgroupId : checkgroupIds) {
                Map map = new HashMap();
                map.put("setmealId",setmealId);
                map.put("checkgroupId",checkgroupId);
                setMealDao.setSetmealAndCheckGroup(map);
            }
        }
    }

    /**
     * 分页条件查询
     *
     * @param queryPageBean 封装的查询条件
     * @return 分页结果封装对象
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {

        //调用Mybatis分页插件方法(自动帮我们做分页)
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());

        //调用Dao, 获得Page对象(page对象是分页插件自带的, 包含List, total...)
        Page<Setmeal> page = setMealDao.findPage(queryPageBean.getQueryString());

        return new PageResult(page.getTotal(), page.getResult());
    }
}
