package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;

import java.util.List;
import java.util.Map;
/**
 * @Description:
 * @Author: yp
 */
public interface CheckGroupDao {

    void add(CheckGroup checkGroup);

    void setCheckGroupAndCheckItem(Map map);

    Page<CheckGroup> findConditions(String queryString);

    CheckGroup findById(Integer id);

    List<Integer> findCheckItemIdsByCheckGroupId(Integer checkGroupId);

    void edit(CheckGroup checkGroup);

    void deleteAssociation(Integer checkGroupId);

    List<CheckGroup> findAll();

    void deleteById(Integer id);


    void deleteCheckItem(Integer id);
}

