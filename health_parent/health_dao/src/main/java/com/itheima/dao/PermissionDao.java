package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Permission;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Description:
 * @Author: yp
 */
public interface PermissionDao {

    /**
     * 根据角色id查询出权限
     * @param roleId
     * @return
     */
    Set<Permission> findByRoleId(Integer roleId);

    /**
     * 权限管理分页查询
     * @param queryString
     * @return
     */
    Page<Permission> findConditions(String queryString);

    /**
     * 保存权限
     * @param permission
     */
    void add(Permission permission);

    /**
     * 向t_role_permission保存数据
     * @param map
     */
    void setPermissionAndRole(Map map);

    /**
     * 根据id查询权限
     * @param id
     * @return
     */
    Permission findById(Integer id);

    /**
     * 根据权限的id查询出当前权限关联的角色的Id(查询中间表t_role_permission)
     * @param permissionId
     * @return
     */
    List<Integer> findRoleIdsByPerssionId(Integer permissionId);

    /**
     * 调用dao 更新t_permissiont
     * @param permission
     */
    void edit(Permission permission);

    /**
     * 调用dao 先把t_permissiont关联的检查项给删除(t_role_permission)
     * @param id
     */
    void deleteAssociation(Integer id);

    /**
     * 删除权限
     * @param id
     */
    void deleteByIdPermission(Integer id);
}
