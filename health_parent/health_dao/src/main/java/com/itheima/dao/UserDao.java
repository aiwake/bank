package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.User;

import java.util.Map;

/**
 * @Description:
 * @Author: yp
 */
public interface UserDao {
    /**
     * 根据用户名查询
     *
     * @param username
     * @return
     */
    User findByUsername(String username);

    /**
     * 新增用户
     *
     * @param user
     */
    void add(User user);

    /**
     * @param map
     */
    void setUserAndRoleMidTable(Map map);

    /**
     * 分页条件查询
     *
     * @param queryString
     * @return
     */
    Page<User> findPage(String queryString);

    /**
     * 根据用户id查询用户
     *
     * @param id
     * @return
     */
    User findById(int id);

    /**
     * 更新用户
     *
     * @param user
     */
    void edit(User user);

    /**
     * 通过id删除User关联的中间表
     *
     * @param userId
     */
    void deleteAssociation(Integer userId);

    /**
     * 通过id删除用户
     *
     * @param id
     */
    void deleteById(Integer id);

    /**
     * 修改密码
     *
     * @param user
     */
    void editPassword(User user);


    /**
     * 将头像图片名称存入数据库
     * @param user
     */
    void updateUserImgByFilename(User user);
}
