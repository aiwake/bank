package com.itheima.dao;

import com.itheima.pojo.CheckGroup;
import com.github.pagehelper.Page;
import com.itheima.pojo.Setmeal;

import java.util.List;
import java.util.Map;
/**
 * @Description:
 * @Author: yp
 */
public interface SetMealDao {

    void add(Setmeal setmeal);

    void setSetmealAndCheckGroup(Map map);


    List<Setmeal> getSetmeal();

    Setmeal findById(Integer id);

    List<Map<String,Object>> getSetmealReport();

    Page<Setmeal> findPage(String queryString);

    List<CheckGroup> findByCheckGroupId(Integer id);


    void deleteById(Integer id);
}
