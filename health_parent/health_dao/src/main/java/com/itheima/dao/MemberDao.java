package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Member;
import com.itheima.pojo.User;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface MemberDao {
    public List<Member> findAll();

    public Page<Member> selectByCondition(String queryString);

    public void add(Member member);

    public void deleteById(Integer id);

    public Member findById(Integer id);

    public Member findByTelephone(String telephone);

    public void edit(Member member);

    public Integer findMemberCountBeforeDate(String date);

    public Integer findMemberCountByDate(String date);

    public Integer findMemberCountAfterDate(String date);

    public Integer findMemberTotalCount();


    Page<Map> findPage(String queryString);

    /**
     * 查询性别名称 以及性别个数
     * @return
     */
    List<Map<String, Object>> getSexReport();

    /**
     * 查询年龄段名称 以及年龄段个数
     * @return
     */
    List<Map<String, Object>> getAgeReport();

    List<Map<String,Object>> findMemberByTime(Map<String, Date> map);
}
