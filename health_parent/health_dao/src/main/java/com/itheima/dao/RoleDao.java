package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Description:
 * @Author: yp
 */
public interface RoleDao {

    Set<Role> findByUid(Integer userId);
    Page<Role> findConditions(String queryString);
    // ok
    void add(Role role);
    // ok
    void setRoleAndMenu(Map map);
    // ok
    void setRoleAndPermission(Map map);
    // ok
    void edit(Role role);
    // ok
    void deleteRoleAndMenu(Integer id);
    // ok
    void deleteRoleAndPermission(Integer id);

    /**
     * 根据ID查询用户是否使用角色 ok
     * @param id
     * @return
     */
    long findCountByRoleId(Integer id);
    // ok
    void deleteById(Integer id);
    // 根据ID查询菜单
//    LinkedHashSet<Menu> findMenuById(Integer id);
    // 根据ID查询权限
//    Set<Permission> findPermissionById(Integer id);

    List<Integer> findPermissionIds(Integer id);

    List<Integer> findMenuIds(Integer id);

    Set<Permission> findPermissions();

    LinkedHashSet<Menu> findMenus();

    Role findById(Integer id);

    void deleteUserAndRole(Integer id);

    List<Role> findAll();

    /**
     * 根据userId查询角色的roleIds
     *
     * @param userId
     * @return
     */
    List<Integer> findRoleIdsByUserId(int userId);
}
