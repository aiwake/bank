package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Role;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;


public interface MenuDao {


    Page<Menu> findConditions(String queryString);

    /**
     * 查询所有
     *
     * @return
     */
    List<Role> findAll();

    void setMenuIdAndCheckRoleIds(Map map);

    List<Menu> findParentMenuList();

    Menu findPathByParentMenuId(Integer id);

    void addFirst(Menu menu);

    void addSecond(Menu menu);
    List<Integer> findCheckRoleIds(Integer id);

    void editFirst(Menu menu);

    void editSecond(Menu menu);

    void deleteMenuAdnRole(Integer id);

    void setRoleAndMenu(Map map);


    void delById(Integer id);

    LinkedHashSet<Menu> findByRoleId(int id);


    LinkedHashSet findByPid(Integer id);
}