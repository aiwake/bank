package com.itheima.security;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Description:
 * @Author: yp
 */
@Controller
@RequestMapping("/hello")
public class HelloController {

    @RequestMapping("/add")
    @PreAuthorize("hasAuthority('add')")
    public String add(){
        System.out.println("add...");
        return null;
    }

    @RequestMapping("/delete")
    @PreAuthorize("hasAuthority('delete')")
    public String delete(){
        System.out.println("delete...");
        return null;
    }

    @RequestMapping("/update")
    @PreAuthorize("hasAuthority('update')")
    public String update(){
        System.out.println("update...");
        return null;
    }
}
