package com.itheima.test;

import com.itheima.pojo.User;
import com.itheima.utils.MD5Utils;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @Description:
 * @Author: yp
 */
public class PasswordTest {

    @Test
    public void fun01(){
        //用户注册的时候, 用户名, 密码
        User user = new User();
        user.setUsername("zs");
        user.setPassword("a123456");

        //加盐了  user.getUsername()+"_"
        String password = MD5Utils.md5(user.getUsername()+"_"+user.getPassword());
        System.out.println(password);
    }

    @Test
    //$2a$10$QzpDPhZyNwZwK7MK0L0aEeOLS5xR0ih8jO7IvhE4eCt8KiIER37Zu
    //$2a$10$A/0IKXxDjo/qgVtFKbSoAOr8oLO.x8WRn3stM52SiJQ/jmog7frdC
    //$2a$10$TTqTQ61tMwiXLQnDKptWTO9TGRri1ZbUBagi6.t99.Qi9yQo3R4wu
    //$2a$10$oq1yVYWPwiCDZr1bmrERpetbB7Qz2rGTb9kV8xferI.ONhuGcCbyq
    public void fun02(){
        String password = "123456";
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        password =   encoder.encode(password);
        System.out.println(password);
    }

    @Test
    //根据加密后的密码, 得出盐
    //把原始密码+盐算一下(不是像我们直接算的)
    //再比较(不是equal)
    public void fun03(){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        boolean success = encoder.matches("123456", "$2a$10$TTqTQ61tMwiXLQnDKptWTO9TGRri1ZbUBagi6.t99.Qi9yQo3R4wu");
        System.out.println(success);

    }



}
